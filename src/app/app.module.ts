import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { Angulartics2Module, Angulartics2GoogleAnalytics } from 'angulartics2';
import { GalleryModule, GalleryConfig } from 'ng-gallery';
import { RoadtripsModule, RoadtripsDataService, RoadtripsDataObjectService } from 'agm-roadtrips/src';

import { EditorModule } from './editor/editor.module';
import { AppComponent } from './app.component';
import { RoadtripsComponent } from './roadtrips/roadtrips.component';
import { RoadtripComponent } from './roadtrip/roadtrip.component';
import { RoadtripGalleryComponent } from './roadtrip-gallery/roadtrip-gallery.component';
import { ExternalDataResolver } from './data/index';
import { ImportComponent } from './import/import.component';
import { UIService } from './shared/index';

import { data } from './data/data';
import { routes } from './app.routes';
import { HomeComponent } from './home/home.component';
import { PlansComponent } from './plans/plans.component';
import { BlogComponent } from './blog/blog.component';
import { AboutComponent } from './about/about.component';

import { environment } from '../environments/environment';
import { RoadtripPageComponent } from './roadtrip-page/roadtrip-page.component';


console.log(
  'App version:', environment.version + ',',
  'environment:', environment.env + ',',
  'hash:', environment.commitHash + ',',
  'date:', environment.commitDate
);

export const galleryConfig: GalleryConfig = {
  'style': {
    'background': '#121519',
    'width': '100%',
    'height': '100%',
    'padding': '1em'
  },
  'animation': 'fade',
  'loader': {
    'width': '50px',
    'height': '50px',
    'position': 'center',
    'icon': 'oval'
  },
  'description': {
    'position': 'bottom',
    'overlay': false,
    'text': true,
    'counter': true
  },
  // 'navigation': true,
  // 'bullets': false,
  'player': {
    'autoplay': false,
    'speed': 3000
  },
  'thumbnails': {
    'width': 120,
    'height': 90,
    'position': 'bottom',
    'space': 20
  }
};

export function RoadtripsDataObjectServiceFactory() {
  return new RoadtripsDataObjectService(data);
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    Angulartics2Module.forRoot([ Angulartics2GoogleAnalytics ]),
    GalleryModule.forRoot(galleryConfig),
    RoadtripsModule,
    EditorModule
  ],
  declarations: [
    AppComponent,
    RoadtripsComponent,
    RoadtripComponent,
    RoadtripGalleryComponent,
    RoadtripPageComponent,
    ImportComponent,
    HomeComponent,
    PlansComponent,
    BlogComponent,
    AboutComponent
  ],
  providers: [
    UIService,
    ExternalDataResolver,
    {
      provide: RoadtripsDataObjectService,
      useFactory: RoadtripsDataObjectServiceFactory
    },
    {
      provide: RoadtripsDataService,
      useExisting: RoadtripsDataObjectService
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
