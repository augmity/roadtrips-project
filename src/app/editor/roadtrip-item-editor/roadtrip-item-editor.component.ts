import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { RoadtripItem, RoadtripItemTypeDisplayNames } from 'agm-roadtrips/src';


@Component({
  selector: 'roadtrip-item-editor',
  templateUrl: './roadtrip-item-editor.component.html',
  styleUrls: ['./roadtrip-item-editor.component.scss']
})
export class RoadtripItemEditorComponent implements OnInit {

  @Input()
    get model(): RoadtripItem {
      return this._model;
    }
    set model(value: RoadtripItem) {
      this._model = value;
      this.form.patchValue(this.model);
    }
  @Output() modelChange = new EventEmitter<RoadtripItem>();
  form: FormGroup;
  roadtripItemTypeDisplayNames = RoadtripItemTypeDisplayNames;
  imageUri: string;
  private _model: RoadtripItem;

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      id: '',
      title: '',
      description: '',
      date: '',
      imageUri: '',
      thumbnailUri: '',
      author: '',
      gpsCoord: '',
      type: ''
    });

    this.form.valueChanges
      .subscribe(value => {
        this.imageUri = value.thumbnailUri;
        this.modelChange.next(value);
      });
  }

  ngOnInit() {

  }
}
