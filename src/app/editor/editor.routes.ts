import { Routes } from '@angular/router';

import { EditorComponent } from './editor.component';
import { RoadtripEditorComponent } from './roadtrip-editor/roadtrip-editor.component';
import { RoadtripItemEditorComponent } from './roadtrip-item-editor/roadtrip-item-editor.component';
import { EditorDefaultViewComponent } from './editor-default-view/editor-default-view.component';

export const EditorRoutes: Routes = [
  { path: 'editor',
    component: EditorComponent,
    children: [
      { path: 'roadtrip/:id', component: RoadtripEditorComponent },
      { path: 'roadtrip-item/:id', component: RoadtripItemEditorComponent },
      { path: 'default', component: EditorDefaultViewComponent },
      // { path: '', pathMatch: 'full', redirectTo: '/default' }
    ]
  }
];
