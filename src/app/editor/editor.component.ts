import { Component, OnInit } from '@angular/core';
import { TreeNode } from 'primeng/components/common/treenode';

import { RoadtripsDataService, RoadtripsDataObjectService, Roadtrip, RoadtripItem, RoadtripItemType } from 'agm-roadtrips/src';


@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {

    tree: TreeNode[];
    selectedItem: TreeNode;
    loadFromLocalState: boolean;
    selectedRoadtrip: Roadtrip;
    selectedRoadtripItem: RoadtripItem;

    constructor(private roadtripsDataService: RoadtripsDataService, ) {}

    ngOnInit() {
      this.loadFromLocalState = (localStorage.getItem('loadFormLocal') === '1');

      this.reloadTree();
    }

    nodeSelect(event) {
      if (event.node.children) {
        this.selectedRoadtrip = event.node.data;
        this.selectedRoadtripItem = null;
      } else {
        this.selectedRoadtrip = null;
        this.roadtripsDataService.getRoadtripItem(event.node.data)
          .subscribe(entity => {
            this.selectedRoadtripItem = entity;
          })
      }
    }

    nodeUnselect(event) {
      // console.log('event', event);
    }

    expandAll() {
      this.tree.forEach( node => {
        this.expandRecursive(node, true);
      });
    }

    collapseAll() {
      this.tree.forEach( node => {
        this.expandRecursive(node, false);
      });
    }

    loadFromLocal(state: boolean) {
      localStorage.setItem('loadFormLocal', (state) ? '1' : '0');
      if (state) {
        this.save();
      };
      this.loadFromLocalState = (localStorage.getItem('loadFormLocal') === '1');
    }

    addRoadtrip() {
      this.roadtripsDataService.addRoadtrip({})
        .subscribe(entity => {
          this.tree.push({
            label: entity.title,
            expandedIcon: 'fa-folder-open',
            collapsedIcon: 'fa-folder',
            data: entity,
            children: []
          });
        });
    }

    addRoadtripItem(roadtripId: string) {
      this.roadtripsDataService.addRoadtripItem({ type: RoadtripItemType.Image })
        .subscribe(entity => {
          this.roadtripsDataService.addRoadtripItemToRoadtrip(roadtripId, entity.id)
            .subscribe(_ => {
              const idx = this.tree.findIndex(item => item.data.id === roadtripId);
              if (idx > -1) {
                this.tree[idx].children.push({
                  label: entity.id,
                  expandedIcon: 'fa-file-o',
                  collapsedIcon: 'fa-file-o',
                  data: entity.id
                });
              }
            });
        });
    }

    save() {
      if (this.selectedRoadtrip) {
        this.roadtripsDataService.updateRoadtrip(this.selectedRoadtrip);
        const idx = this.tree.findIndex(item => item.data.id === this.selectedRoadtrip.id);
        if (idx > -1) {
          this.tree[idx] = {
            label: this.selectedRoadtrip.title,
            expandedIcon: 'fa-folder-open',
            collapsedIcon: 'fa-folder',
            data: this.selectedRoadtrip,
            children: []
          };
        }
      }
      if (this.selectedRoadtripItem) {
        this.roadtripsDataService.updateRoadtripItem(this.selectedRoadtripItem);
      }
      localStorage.setItem('localData', JSON.stringify((this.roadtripsDataService as RoadtripsDataObjectService).getData()));
    }

    private reloadTree() {
      this.roadtripsDataService.getRoadtrips()
        .subscribe(items => {
          this.tree = items.map((item: Roadtrip) => {
            return {
              label: item.title,
              expandedIcon: 'fa-folder-open',
              collapsedIcon: 'fa-folder',
              data: item,
              children: item.items.map(el => {
                return {
                  label: el,
                  expandedIcon: 'fa-file-o',
                  collapsedIcon: 'fa-file-o',
                  data: el
                };
              })
            };
          });
        });
    }

    private expandRecursive(node: TreeNode, isExpand: boolean) {
      node.expanded = isExpand;
      if (node.children) {
        node.children.forEach( childNode => {
          this.expandRecursive(childNode, isExpand);
        });
      }
    }
}
