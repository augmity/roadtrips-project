import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorDefaultViewComponent } from './editor-default-view.component';

describe('EditorDefaultViewComponent', () => {
  let component: EditorDefaultViewComponent;
  let fixture: ComponentFixture<EditorDefaultViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorDefaultViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorDefaultViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
