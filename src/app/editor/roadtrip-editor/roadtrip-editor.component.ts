import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { RoadtripItem } from 'agm-roadtrips/src';


@Component({
  selector: 'roadtrip-editor',
  templateUrl: './roadtrip-editor.component.html',
  styleUrls: ['./roadtrip-editor.component.scss']
})
export class RoadtripEditorComponent implements OnInit {

  @Input()
  get model(): RoadtripItem {
    return this._model;
  }
  set model(value: RoadtripItem) {
    this._model = value;
    this.form.patchValue(this.model);
  }
  @Output() modelChange = new EventEmitter<RoadtripItem>();
  form: FormGroup;
  imageUri: string;
  private _model: RoadtripItem;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      id: [],
      title: [],
      subtitle: [],
      description: [],
      slug: [],
      date: [],
      imageUri: [],
      author: []
    });

    this.form.valueChanges
      .subscribe(value => {
        this.imageUri = value.imageUri;
        this.modelChange.next(value);
      });
  }

  ngOnInit() {

  }
}
