import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { TreeModule } from 'primeng/components/tree/tree';
import { LookupsModule } from 'agm-angular-pack/src/lookups';

import { EditorComponent } from './editor.component';
import { RoadtripEditorComponent } from './roadtrip-editor/roadtrip-editor.component';
import { RoadtripItemEditorComponent } from './roadtrip-item-editor/roadtrip-item-editor.component';
import { EditorDefaultViewComponent } from './editor-default-view/editor-default-view.component';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TreeModule,
    LookupsModule
  ],
  declarations: [
    EditorComponent,
    RoadtripEditorComponent,
    RoadtripItemEditorComponent,
    EditorDefaultViewComponent
  ]
})
export class EditorModule { }
