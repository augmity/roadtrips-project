import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { RoadtripsDataObjectService, RoadtripsDataObject } from 'agm-roadtrips/src';


@Component({
  selector: 'app-import',
  templateUrl: './import.component.html'
})
export class ImportComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router,
              private http: HttpClient, private roadtripsDataObjectService: RoadtripsDataObjectService) { }

  ngOnInit() {
    const dataUri = this.route.snapshot.params['dataUri'];

    if (dataUri) {
      this.http.get<RoadtripsDataObject>(dataUri)
        // .catch(this.handleError)
        .subscribe(data => {
          this.roadtripsDataObjectService.loadDataObject(data);
          localStorage.setItem('dataUri', dataUri);
          this.router.navigate(['/']);
        });
    } else {
      this.router.navigate(['/']);
    }
  }

  // private handleError (error: Response | any) {
  //   let errMsg: string;
  //   if (error instanceof Response) {
  //     const body = error.json() || '';
  //     const err = body.error || JSON.stringify(body);
  //     errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
  //   } else {
  //     errMsg = error.message ? error.message : error.toString();
  //   }
  //   console.error(errMsg);
  //   return Observable.throw(errMsg);
  // }
}
