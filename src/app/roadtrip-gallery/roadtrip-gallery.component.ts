import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { RoadtripsDataService, Roadtrip, RoadtripItem, RoadtripItemType } from 'agm-roadtrips/src';
import { GalleryService, GalleryImage } from 'ng-gallery';
import { Angulartics2 } from 'angulartics2';

import { UIService } from '../shared/index';


export interface GalleryImageEx extends GalleryImage {
  id: string;
}


@Component({
  selector: 'app-roadtrip-gallery',
  templateUrl: './roadtrip-gallery.component.html',
  styleUrls: ['./roadtrip-gallery.component.scss']
})
export class RoadtripGalleryComponent implements OnInit {

  roadtrip: Roadtrip;
  items: RoadtripItem[];
  images: GalleryImageEx[];
  RoadtripItemType = RoadtripItemType;

  constructor(private roadtripsDataService: RoadtripsDataService, private route: ActivatedRoute, private gallery: GalleryService,
              private angulartics2: Angulartics2, private uiService: UIService, private router: Router, public sanitizer: DomSanitizer) { }

  ngOnInit() {
    const slug = this.route.snapshot.params['slug'];

    this.roadtripsDataService.getRoadtripBySlug(slug)
      .subscribe(result => {
        this.roadtrip = result;

        this.roadtripsDataService.getRoadtripItems(this.roadtrip.id)
          .subscribe(roadtripItems => {
            // All items to display
            const items = [];
            for (const item of roadtripItems) {
              if ((item.type === RoadtripItemType.Image) || (item.type === RoadtripItemType.Video)) {
                if (item.description) {
                  items.push({ type: RoadtripItemType.Text, description: item.description});
                }
              };
              items.push(item);
            }
            this.items = items;

            // For gallery component
            this.images = roadtripItems
              .filter(item => item.type === RoadtripItemType.Image)
              .map(item => {
                return {
                  src: item.imageUri,
                  thumbnail: item.thumbnailUri,
                  text: item.title,
                  id: item.id
                  };
              });
            this.gallery.load(this.images);
          });
      });
  }

  galleryItemClick(id: string) {
    const idx = this.images.findIndex(item => item.id === id);
    this.gallery.set(idx);
    this.angulartics2.eventTrack.next({ action: 'roadtripItemClick', properties: { category: 'myCategory', label: 'myLabel' }});
  }

  switchView() {
    this.uiService.roadtripRouteBase.next('roadtrip-page');
    this.router.navigate(['/roadtrip-page', this.roadtrip.id]);
  }
}
