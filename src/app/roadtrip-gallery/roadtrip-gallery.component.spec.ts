import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoadtripGalleryComponent } from './roadtrip-gallery.component';

describe('RoadtripGalleryComponent', () => {
  let component: RoadtripGalleryComponent;
  let fixture: ComponentFixture<RoadtripGalleryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoadtripGalleryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoadtripGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
