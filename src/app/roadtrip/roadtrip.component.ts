import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RoadtripsDataService, Roadtrip, UID } from 'agm-roadtrips/src';


@Component({
  selector: 'app-roadtrip',
  templateUrl: './roadtrip.component.html',
  styleUrls: ['./roadtrip.component.scss']
})
export class RoadtripComponent implements OnInit {

  isFirst = true;
  isLast = true;
  currentId: UID;
  roadtrip: Roadtrip;
  // currentIdx
    get currentIdx(): number {
      return this._currentIdx;
    }
    set currentIdx(value: number) {
      // Can we?
      if (!this.roadtrip || this.roadtrip.items.length <= 0) {
        return;
      }

      // Respect boundries
      if ((value < 0) || (value >= this.roadtrip.items.length)) {
        return;
      }

      this._currentIdx = value;
      this.currentId = this.roadtrip.items[this._currentIdx];
      this.isFirst = (this._currentIdx === 0);
      this.isLast = (this._currentIdx === this.roadtrip.items.length - 1);
    }
  private _currentIdx: number;


  constructor(private roadtripsDataService: RoadtripsDataService, private route: ActivatedRoute) { }

  ngOnInit() {
    const id = this.route.snapshot.params['id'];

    this.roadtripsDataService.getRoadtrip(id)
      .subscribe(result => {
        this.roadtrip = result;
        this.currentIdx = 0;
      });
  }

  prev() {
    this.currentIdx--;
  }

  next() {
    this.currentIdx++;
  }
}
