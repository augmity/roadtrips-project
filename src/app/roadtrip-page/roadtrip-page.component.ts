import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RoadtripsDataService, Roadtrip, RoadtripItem } from 'agm-roadtrips/src';

import { UIService } from '../shared/index';


@Component({
  selector: 'app-roadtrip-page',
  templateUrl: './roadtrip-page.component.html',
  styleUrls: ['./roadtrip-page.component.scss']
})
export class RoadtripPageComponent implements OnInit {

  roadtrip: Roadtrip;
  roadtripItems: RoadtripItem[];

  constructor(private roadtripsDataService: RoadtripsDataService, private route: ActivatedRoute, private uiService: UIService) { }

  ngOnInit() {
    const slug = this.route.snapshot.params['slug'];

    this.roadtripsDataService.getRoadtripBySlug(slug)
      .subscribe(result => {
        this.roadtrip = result;

        this.roadtripsDataService.getRoadtripItems(this.roadtrip.id)
          .subscribe(roadtripItems => {
            this.roadtripItems = roadtripItems;
          });
      });
  }
}
