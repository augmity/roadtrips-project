import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { RoadtripsDataObjectService, RoadtripsDataObject } from 'agm-roadtrips/src';


@Injectable()
export class ExternalDataResolver implements Resolve<boolean> {

  constructor(private http: HttpClient, private roadtripsDataObjectService: RoadtripsDataObjectService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<boolean> {
    // Try local storage, if requested
    if (localStorage.getItem('loadFormLocal') === '1') {
      const localData = JSON.parse(localStorage.getItem('localData'));
      this.roadtripsDataObjectService.loadDataObject(localData);
      return Observable.of(true);
    }

    // Try remote data location
    const dataUri = localStorage.getItem('dataUri');
    if (!dataUri) {
      return Observable.of(true);
    } else {
      return this.http.get<RoadtripsDataObject>(dataUri)
        .map(data => {
          this.roadtripsDataObjectService.loadDataObject(data);
          return true;
        })
        // .catch(this.handleError);
    }
  }

  // private handleError (error: HttpErrorResponse | any) {
  //   let errMsg: string;
  //   if (error instanceof HttpErrorResponse) {
  //     const body = error.json() || '';
  //     const err = body.error || JSON.stringify(body);
  //     errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
  //   } else {
  //     errMsg = error.message ? error.message : error.toString();
  //   }
  //   console.error(errMsg);
  //   return Observable.throw(errMsg);
  // }
}
