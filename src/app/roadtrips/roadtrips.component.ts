import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { RoadtripsDataService, Roadtrip } from 'agm-roadtrips/src';

import { UIService } from '../shared/index';


@Component({
  selector: 'app-roadtrips',
  templateUrl: './roadtrips.component.html',
  styleUrls: ['./roadtrips.component.scss']
})
export class RoadtripsComponent implements OnInit {

  items: Observable<Roadtrip[]>;
  roadtripRouteBase: string;

  constructor(private roadtripsDataService: RoadtripsDataService, private uiService: UIService) { }

  ngOnInit() {
    this.items = this.roadtripsDataService.getRoadtrips();

    this.uiService.roadtripRouteBase.subscribe(result => { this.roadtripRouteBase = '/' + result; });
  }
}
