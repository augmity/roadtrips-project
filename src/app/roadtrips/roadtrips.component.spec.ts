import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoadtripsComponent } from './roadtrips.component';

describe('RoadtripsComponent', () => {
  let component: RoadtripsComponent;
  let fixture: ComponentFixture<RoadtripsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoadtripsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoadtripsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
