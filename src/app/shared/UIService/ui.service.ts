import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


@Injectable()
export class UIService {

  roadtripRouteBase = new BehaviorSubject<string>('roadtrip-gallery');
}
