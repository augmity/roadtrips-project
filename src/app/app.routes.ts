import { Routes } from '@angular/router';

import { EditorRoutes } from './editor/editor.routes';

import { RoadtripsComponent } from './roadtrips/roadtrips.component';
import { RoadtripComponent } from './roadtrip/roadtrip.component';
import { RoadtripGalleryComponent } from './roadtrip-gallery/roadtrip-gallery.component';
import { RoadtripPageComponent } from './roadtrip-page/roadtrip-page.component';
import { ImportComponent } from './import/import.component';
import { ExternalDataResolver } from './data/index';
import { AboutComponent } from './about/about.component';
import { BlogComponent } from './blog/blog.component';
import { HomeComponent } from './home/home.component';
import { PlansComponent } from './plans/plans.component';

export const routes: Routes = [
  { path: '',
    resolve: {
      externalData: ExternalDataResolver
    },
    children: [
      ...EditorRoutes,
      { path: 'roadtrips', component: RoadtripsComponent },
      { path: 'roadtrip/:id', component: RoadtripComponent },
      { path: 'roadtrip-gallery/:slug', component: RoadtripGalleryComponent },
      { path: 'roadtrip-page/:slug', component: RoadtripPageComponent },
      { path: 'import/:dataUri', component: ImportComponent },
      { path: 'about', component: AboutComponent },
      { path: 'blog', component: BlogComponent },
      { path: 'plans', component: PlansComponent },
      { path: 'home', pathMatch: 'full', redirectTo: '/roadtrips' },
      { path: '', pathMatch: 'full', redirectTo: '/roadtrips' }
    ]
  }
];
