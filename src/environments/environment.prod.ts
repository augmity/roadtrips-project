export const environment = {
  env: 'prod',
  production: true,
  version: '0.6.4',
  commitHash: '221faf135798d8548d705d844437f07fc6abf67e',
  commitDate: 'Wed Oct 04 2017 22:25:15 GMT-0700 (Pacific Daylight Time)'
};
