// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  env: 'dev',
  production: false,
  version: '0.6.4',
  commitHash: '221faf135798d8548d705d844437f07fc6abf67e',
  commitDate: 'Wed Oct 04 2017 22:25:15 GMT-0700 (Pacific Daylight Time)'
};
