# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.6.4"></a>
## [0.6.4](https://bitbucket.org/augmity/roadtrips-project/compare/v0.6.3...v0.6.4) (2017-12-31)



<a name="0.6.3"></a>
## [0.6.3](https://bitbucket.org/augmity/roadtrips-project/compare/v0.6.2...v0.6.3) (2017-10-05)


### Bug Fixes

* IE11 polyfill issue and "canadian graffiti" updates ([52d3306](https://bitbucket.org/augmity/roadtrips-project/commits/52d3306))



<a name="0.6.2"></a>
## [0.6.2](https://bitbucket.org/augmity-git/roadtrips-project/compare/v0.6.1...v0.6.2) (2017-10-03)


### Bug Fixes

* story update and small UI fixes ([f1c3854](https://bitbucket.org/augmity-git/roadtrips-project/commits/f1c3854))



<a name="0.6.1"></a>
## [0.6.1](https://bitbucket.org/augmity-git/roadtrips-project/compare/v0.6.0...v0.6.1) (2017-09-17)



<a name="0.6.0"></a>
# [0.6.0](https://bitbucket.org/augmity-git/roadtrips-project/compare/v0.5.0...v0.6.0) (2017-09-12)


### Features

* **data:** Total Solar Eclipse updated ([69cd2c2](https://bitbucket.org/augmity-git/roadtrips-project/commits/69cd2c2))
* **data:** Total Solar Eclipse updated; small code changes ([d3c6b3a](https://bitbucket.org/augmity-git/roadtrips-project/commits/d3c6b3a))



<a name="0.5.0"></a>
# [0.5.0](https://bitbucket.org/augmity-git/roadtrips-project/compare/v0.4.0...v0.5.0) (2017-09-08)


### Features

* editor changes, UI improvements, "about" view added ([c2b80da](https://bitbucket.org/augmity-git/roadtrips-project/commits/c2b80da))
* small UI changes, data updated ([5a8cb53](https://bitbucket.org/augmity-git/roadtrips-project/commits/5a8cb53))



<a name="0.4.0"></a>
# [0.4.0](https://bitbucket.org/augmity-git/roadtrips-project/compare/v0.3.0...v0.4.0) (2017-08-30)


### Features

* added slugs (friendly urls) for roadtrips ([975340a](https://bitbucket.org/augmity-git/roadtrips-project/commits/975340a))



<a name="0.3.0"></a>
# [0.3.0](https://bitbucket.org/augmity-git/roadtrips-project/compare/v0.2.0...v0.3.0) (2017-08-29)


### Features

* data editor added ([ff34936](https://bitbucket.org/augmity-git/roadtrips-project/commits/ff34936))



<a name="0.2.0"></a>
# [0.2.0](https://bitbucket.org/augmity-git/roadtrips-project/compare/v0.1.0...v0.2.0) (2017-08-16)


### Bug Fixes

* added missing pieces of code ([d045dca](https://bitbucket.org/augmity-git/roadtrips-project/commits/d045dca))
* angulartics2 setup fixed ([ff01cf0](https://bitbucket.org/augmity-git/roadtrips-project/commits/ff01cf0))


### Features

* angulartics2 added ([59f7429](https://bitbucket.org/augmity-git/roadtrips-project/commits/59f7429))
* **ui:** added ability to switch athe roadtrip view (page/gallery) ([285c31d](https://bitbucket.org/augmity-git/roadtrips-project/commits/285c31d))



<a name="0.1.0"></a>
# 0.1.0 (2017-08-14)


### Features

* **view:** gallery view added ([cbe1f48](https://bitbucket.org/augmity-git/roadtrips-project/commits/cbe1f48))
* **view:** Plans view added ([306ce07](https://bitbucket.org/augmity-git/roadtrips-project/commits/306ce07))
* **view:** roadtrip view implemented ([da09f21](https://bitbucket.org/augmity-git/roadtrips-project/commits/da09f21))
* versioning scripts added ([72040d7](https://bitbucket.org/augmity-git/roadtrips-project/commits/72040d7))
* **view:** RoadtripPage view added ([fd53910](https://bitbucket.org/augmity-git/roadtrips-project/commits/fd53910))



<a name="0.1.0"></a>
# 0.1.0 (2017-08-14)


### Features

* **view:** gallery view added ([cbe1f48](https://bitbucket.org/augmity-git/roadtrips-project/commits/cbe1f48))
* **view:** Plans view added ([306ce07](https://bitbucket.org/augmity-git/roadtrips-project/commits/306ce07))
* **view:** roadtrip view implemented ([da09f21](https://bitbucket.org/augmity-git/roadtrips-project/commits/da09f21))
* versioning scripts added ([72040d7](https://bitbucket.org/augmity-git/roadtrips-project/commits/72040d7))
* **view:** RoadtripPage view added ([fd53910](https://bitbucket.org/augmity-git/roadtrips-project/commits/fd53910))
