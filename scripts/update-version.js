var replace = require("replace");
var git = require('git-rev-sync');


function doReplace(propertyName, replaceWith) {
  var regex = new RegExp(propertyName + ": '.*'", "g");
  replace({
    regex: regex,
    replacement: propertyName + ": '" + replaceWith + "'",
    paths: [
      './src/environments/environment.ts',
      './src/environments/environment.prod.ts'
    ],
    recursive: true
  });
}

var newVersion = process.env['npm_package_version'];
var commitHash = git.long();
var commitDate = git.date();

console.log('newVersion: ' + newVersion);
console.log('commitHash: ' + commitHash);
console.log('commitDate: ' + commitDate);

doReplace('version', newVersion);
doReplace('commitHash', commitHash);
doReplace('commitDate', commitDate);
