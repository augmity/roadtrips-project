import { RoadtripsProjectPage } from './app.po';

describe('roadtrips-project App', () => {
  let page: RoadtripsProjectPage;

  beforeEach(() => {
    page = new RoadtripsProjectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
